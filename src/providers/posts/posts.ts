import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environment';

/*
  Generated class for the PostsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class PostsProvider {
		  api_url= environment.site_url+environment.posts_url;
      // embed_url= environment.site_url+environment.posts_embed_url;

  constructor(public http: HttpClient) {
    console.log('Hello PostsProvider Provider');
  }
  getPosts(){
  	return this.http.get(this.api_url);
  }

  // getPostsEmbed(){
  //   return this.http.get(this.embed_url);
  // }
}
