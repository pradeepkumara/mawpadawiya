import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../environment';

/*
  Generated class for the CategorysProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CategorysProvider {
	  // api_url= environment.site_url+environment.categories_url;
    api_url= environment.site_url+environment.categories_url;
    url= environment.site_url+environment.post_by_categorie_url;
    
    constructor(public http: HttpClient) {
      console.log('Hello CategorysProvider Provider');
    }


    getCategorys(){
    	return this.http.get(this.api_url);
    }
    getPostsByCategoryId(id){
      return this.http.get(this.url+id);

    }

}
