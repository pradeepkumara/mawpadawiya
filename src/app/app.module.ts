import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
 
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { Splash } from '../pages/splash/splash';
import { AuthProvider } from '../providers/auth/auth';
import { QuotesProvider } from '../providers/quotes/quotes';
import { HttpClientModule } from '@angular/common/http';
import { CategorysProvider } from '../providers/categorys/categorys';
import { PostsProvider } from '../providers/posts/posts';



 
@NgModule({
  declarations: [
    MyApp,
    HomePage,
    Splash
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    Splash
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler,
     useClass: IonicErrorHandler},
    AuthProvider,
    QuotesProvider,
    CategorysProvider,
    PostsProvider
  ]
})
export class AppModule {}