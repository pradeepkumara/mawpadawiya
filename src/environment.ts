export const environment = {
	site_url : 'https://mawpadawiya.000webhostapp.com',
	quotes_url : '/wp-json/wp/v2/quotes',
	posts_url : '/wp-json/wp/v2/posts',
	categories_url : '/wp-json/wp/v2/categories',
	post_by_categorie_url : '/wp-json/wp/v2/posts/?categories=',
	// posts_embed_url :'/wp-json/wp/v2/posts?_embed',
	// post_by_post_id_url : '/wp-json/wp/v2/?posts=',
	jwt_url : '/jwt-auth/v1/token'
}