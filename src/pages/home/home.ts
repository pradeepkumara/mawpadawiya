import { Component } from '@angular/core';
// import { NavController } from 'ionic-angular';
import { enableProdMode } from "@angular/core";
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { QuotesProvider } from "../../providers/quotes/quotes";
import { CategorysProvider } from "../../providers/categorys/categorys";

enableProdMode();


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  quotes;
  categorys;

  constructor(
      public navCtrl: NavController,
      public navParams: NavParams,
      private quotesProvider: QuotesProvider,
      private categorysProvider: CategorysProvider
    ) {

     categorysProvider.getCategorys().subscribe(data => {
      this.categorys = data;

    });

     quotesProvider.getQuotes().subscribe(data => {
      this.quotes = data;
    });
  }

  onShowCategoryDetails(cate){
      this.categorysProvider.getPostsByCategoryId(cate.id).subscribe(data => {
       // this.navCtrl.push('CategoryDetailsPage', {catagorys:data});
       this.navCtrl.push('CategoryDetailsPage', {categorys:data, cate:cate});
      });                              
  }
  onShowQuoteDetails(quote){
    this.navCtrl.push('CategoryDetailsPage', {quotes:quote});
  }

	findData(){
	}
	clicked(event){
    

  }



}