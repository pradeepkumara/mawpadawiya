import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { CategorysProvider } from "../../providers/categorys/categorys";
import { PostsProvider } from "../../providers/posts/posts";


/**
 * Generated class for the PostsDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-posts-details',
  templateUrl: 'posts-details.html',
})
export class PostsDetailsPage {

	post;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private categorysProvider: CategorysProvider,
    private postsProvider: PostsProvider
     ) {
  	      
  	this.post = this.navParams.get('posts');
    console.log(this.post );
  	// postsProvider.getPostsByCategoryId(this.categoryId).subscribe(data => {
   //    this.posts = data;
   //  });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PostsDetailsPage');
  }

}
