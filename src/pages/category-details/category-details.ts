import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { CategorysProvider } from "../../providers/categorys/categorys";
import { PostsProvider } from "../../providers/posts/posts";

/**
 * Generated class for the CategoryDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-category-details',
  templateUrl: 'category-details.html',
})
export class CategoryDetailsPage {

  cate;
	quotes;
  posts;
  items:any=[];
  

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private categorysProvider: CategorysProvider,
    private postsProvider: PostsProvider
    ) {

      this.posts = navParams.get('categorys');
      this.cate = navParams.get('cate');
      // this.postsProvider.getPostsEmbed().subscribe(data => {
      console.log(this.posts);
      //     this.items = data;
      //   });
      
  }
  // postsEmbed(){
  //   this.postsProvider.getPostsEmbed().subscribe(data => {
  //     console.log(data);
  //         this.items = data;
  //       });
  // }

  getPostsByCategoryId(post){
    this.navCtrl.push('PostsDetailsPage', {posts:post});
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CategoryDetailsPage');
  }

}

